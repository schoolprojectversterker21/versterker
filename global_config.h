#ifndef _CONFIG_MAIN_FILE
#define _CONFIG_MAIN_FILE

#define HIGH 1
#define LOW 0
#define DIGITAL 0;
#define ANALOG 1;

#define _XTAL_FREQ    4000000

#define ROTARY_STATE_SAVE_LOCATION  0x01                                        // Save location eeprom
#define ROTARY_CHANNEL_COUNT_SAVE_LOCATION 0x02                                 // Save location eeprom
#define ADC_STATE_REG_SAVE_LOCATION 0x03                                        // Save location eeprom

#define ADC_MAX_VOLUME          41                                              // Max possible volume ADC
#define ADC_BASE_MIN            0                                               // absolute possible lowest volume
#define ADC_BASE_MAX            1023                                            // Absolute possible maximum value
#define ADC_DEADZONE_DIV        4                                               // How big should it be?
#define ADC_MAX_STEP_SIZE       ((ADC_BASE_MAX - ADC_BASE_MIN)/ADC_MAX_VOLUME)  // Get the step size
#define ADC_DEADZONE_SIZE       8                                               // Actual deadzone size

#define IR_PIN                  PORTBbits.RB0                                   // IR pin
#define IR_RESOLUTION           12                                              /*The maximum time we're willing to wait for a signal*/
#define IR_BASE_POSITIVE_PULSE_LENGTH 50                                        // Max length.. PROTOTYPE!
#define IR_ACTIVE_LOW_HIGH      0                                               // Low length.. PROTOTYPE!

// Char translation tables for readability
#define CHAR_ZERO 0
#define CHAR_ONE 1
#define CHAR_TWO 2
#define CHAR_THREE 3
#define CHAR_FOUR 4
#define CHAR_FIVE 5
#define CHAR_SIX 6
#define CHAR_SEVEN 7
#define CHAR_EIGHT 8
#define CHAR_NINE 9
#define CHAR_V 10
#define CHAR_O 11
#define CHAR_L 12
#define CHAR_C 13
#define CHAR_H 14
#define CHAR_N 15
#define CHAR_SPACE 16

#endif 