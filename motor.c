
#include <xc.h>
#include "motor.h"
#include "global_config.h"

void initializeMotor()
{
    TRISCbits.TRISC0    = 0;                                                    // 0 = Digital output RC0
    TRISCbits.TRISC1    = 0;                                                    // 0 = Digital output RC1
    PORTCbits.RC0       = 1;                                                    // Active low, 1 = off (Left turn)
    PORTCbits.RC1       = 1;                                                    // Active low, 1 = off (Right turn)
    
}

void setMotorLinks()
{
    PORTCbits.RC0   = 0;                                                        // Active low, 0 = Left Turn on
    
}

void setMotorRechts()
{
    PORTCbits.RC1  = 0;                                                         // Active low, 0 = Left Turn on
    
}

void MotorStop()
{
    PORTCbits.RC0  = 1;                                                       // Active low, 1 = Left turn off
    PORTCbits.RC1  = 1;                                                         // Active low, 1 = Right turn off
}
