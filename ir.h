#ifndef _IR_FILE
#define _IR_FILE

void initializeIR(void);
void pollIR(void);
void doIR(unsigned short seg1);

#endif 