
#include "ir.h"
#include "global_config.h"
#include "motor.h"
#include "pot_meter.h"
#include "rotary_encoder.h"
#include "spi.h"
#include "global_vars.h"
#include <xc.h>

int unsigned short seg1;                                                        // IR code
int active[12], nonActive[12];                                                  // Array's met de cycles van het uitlezen van de code's

void initializeIR()
{
    TRISBbits.TRISB0        = 0b1;                                              // RB0 (IR receiver) is een input
    WPUBbits.WPUB0          = 0b1;                                              // Weak Pull-up Register bit 1= Pull-up is enabled on RB0
    return;
}

void pollIR()
{
    seg1 = 0;                                                                   // Maak seg1 leeg
    
    for (int i = 0; i < IR_RESOLUTION; i++)                                     // Lees alle 12 bits uit naar twee array's
    {
        while(PORTBbits.RB0 == !IR_ACTIVE_LOW_HIGH){                            // Wanneer IR Hoog is
            if(active[i] > 100)                                                 // Als de IR receiver meer dan 100 cycles hoog is
                break;
            active[i]++;                                                        // Verhoog de array
        }
        while(PORTBbits.RB0 == IR_ACTIVE_LOW_HIGH){                             // Wanneer IR Laag is
            if(nonActive[i] > 100)                                              // Als de IR receiver meer dan 100 cycles laag is
                break;
            nonActive[i]++;                                                     // Verhoog de array
        }                          
    }
    
    for(int i = 0; i < IR_RESOLUTION; i++)                                      // Schrijf de 12 bits naar seg1
    {
        if(nonActive[i] > 10)                                                   // Voor de testbank afstandsbediening moet dit op 10 staan
        {
            seg1 = ( seg1 << 1 ) | 0b1;                                         // Voeg een 1 aan seg1
        }else{
            seg1 = ( seg1 << 1 ) | 0b0;                                         // Voeg een 0 aan seg1
        }
        nonActive[i] = 0;                                                       // Reset array nonActive
        active[i] = 0;                                                          // Reset array active
    }
    
    doIR(seg1);                                                                 // Ga naar de functie doIR en geef seg 1 mee
}


void doIR(unsigned short seg1)
{
    if( seg1 == 0b00000010100100000)                                            // Als er op volume + is gedrukt
    {
        if (ADC_STATE_REG != 40)                                                // Als het volume lager dan 40 is
        {
            setMotorRechts();                                                   // Draai de motor naar rechts
            MotorStop();                                                        // Stop de motor
        }
        spiSendVolume();                                                        // Verstuur de volumestand via SPI
    }
    
    if( seg1 == 0b0000010100010000)                                             // Als er op volume + is gedrukt
    {
        if (ADC_STATE_REG != 0x00)                                              // Als het volume hoger dan 0 is
        {
            setMotorLinks();                                                    // Draai de motor naar links
            MotorStop();                                                        // Stop de motor
        }
        spiSendVolume();                                                        // Verstuur de volumestand via SPI
    }
    
    if( seg1 == 0b0000010010010000)                                             // Als er op channel 1 is gedrukt
    {
        ROTARY_CHANNEL_COUNT = 0;                                               // Zet ROTARY_CHANNEL_COUNT op 0 (kanaal 1)
        setChannelLED();                                                        // Roep de functie aan die het kanaal aanpast
    }
    if(seg1 == 0b0000010100001000)                                              // Als er op channel 2 is gedrukt
    {
        ROTARY_CHANNEL_COUNT = 5;                                               // Zet ROTARY_CHANNEL_COUNT op 5 (kanaal 2)
        setChannelLED();                                                        // Roep de functie aan die het kanaal aanpast
    }
    if(seg1 == 0b0000010010100000)                                              // Als er op channel 3 is gedrukt
    {
        ROTARY_CHANNEL_COUNT = 9;                                               // Zet ROTARY_CHANNEL_COUNT op 9 (kanaal 3)
        setChannelLED();                                                        // Roep de functie aan die het kanaal aanpast
    }
    if(seg1 == 0b0000010010001000)                                              // Als er op channel 4 is gedrukt
    {
        ROTARY_CHANNEL_COUNT = 13;                                              // Zet ROTARY_CHANNEL_COUNT op 13 (kanaal 4)
        setChannelLED();                                                        // Roep de functie aan die het kanaal aanpast
    }
}