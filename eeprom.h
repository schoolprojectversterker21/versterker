/* 
 * File:   eeprom.h
 * Author: root
 *
 * Created on January 27, 2019, 11:40 PM
 */

#ifndef _EEPROM_FILE
#define	_EEPROM_FILE
    void eepromWrite( unsigned char addr, unsigned char val );
    unsigned char eepromRead( unsigned char addr ); 
    void saveData( void );
#endif

