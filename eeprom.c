#include "eeprom.h"
#include "global_config.h"
#include "global_vars.h"
#include <pic16f887.h>


void eepromWrite(unsigned char addr, unsigned char val)
{
    char GIE_BIT_VAL = 0;
    
    /*Mandatory steps for EEPROM write*/
    EEADR = addr;                                                               // 1. write adress to EEADR register  
    EEDATA = val;                                                               // 2. write data to EEADR register (max 8 bits)
    
    EECON1bits.EEPGD = 0;                                                       // 0 = acces data memory, 1 = access program memory
    EECON1bits.WREN = 1;                                                        // EEPROM write enable bit, 1 = allows write cycles 
    GIE_BIT_VAL = INTCONbits.GIE; 
    INTCONbits.GIE = 0;                                                         // 0 = disable all interrupt during writing.
    
    /*Initiate writing sequence, these steps are mandatory*/
    EECON2 = 0x55;                                                              // 1st step
    EECON2 = 0xAA;                                                              // 2nd step
    EECON1bits.WR = 1;                                                          // Set write bit to high as we actually want to write..
    
    while(EECON1bits.WR);                                                       // Wait till the write completion
    INTCONbits.GIE = GIE_BIT_VAL;                                               // reset interrupt to state before the write to eeprom
    EECON1bits.WREN = 0;                                                        // 0 = EEPROM write enable bit, 0 = does not allows write cycles.
    return;
}

unsigned char eepromRead(unsigned char addr)
{
    EEADR = addr;                                                               // Get the data from the address
    EECON1bits.EEPGD = 0;                                                       // 0 = acces data memory, 1 = access program memory 
    EECON1bits.RD = 1;                                                          // Prepare for Read   
    asm("nop");                                                                 // waste some time
    asm("nop");                                                                 // Waste some time
    return ( EEDATA );                                                          // return with read byte 
}


void saveData( void )
{
    eepromWrite(ROTARY_STATE_SAVE_LOCATION, ROTARY_STATE_REG);                  // Save the rotary state              
    eepromWrite(ROTARY_CHANNEL_COUNT_SAVE_LOCATION, ROTARY_CHANNEL_COUNT);      // Save the current channel steps
    eepromWrite(ADC_STATE_REG_SAVE_LOCATION, ADC_STATE_REG);                    // Save the adc value
    return;
}