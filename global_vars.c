#include "global_vars.h"
#include <pic16f887.h>
#include "eeprom.h"

void initializeVars( void )
{
    ROTARY_STATE_REG        = eepromRead(ROTARY_STATE_SAVE_LOCATION);                                 // Load the last states of the rotary encoder
    ROTARY_CHANNEL_COUNT    = eepromRead(ROTARY_CHANNEL_COUNT_SAVE_LOCATION);                                 // Load the last step count of the rotary encoder
    ADC_STATE_REG           = eepromRead(ADC_STATE_REG_SAVE_LOCATION);                                 // Load the last volume of the ADC
    PORTA                   = ~( PORTA | ( 0b1 << ( ROTARY_CHANNEL_COUNT >> 2 ))); // Set the channel to the last know channel
    EEPROM_WATCHDOG         = 0;                                                // Set the watchdog timer to 0
}