/* 
 * File:   motor.h
 * Author: root
 *
 * Created on January 26, 2019, 9:17 PM
 */


#ifndef _MOTOR_FILE
#define _MOTOR_FILE

void initializeMotor( void );
void setMotorLinks( void );
void setMotorRechts( void );
void MotorStop(void);

#endif 


