#ifndef _SPI_FILE
#define _SPI_FILE
#include <pic16f887.h>
#include "global_config.h"
#include "global_vars.h"

void initializeSPI( void );
void spiInitDisplay1( void );
void spiInitDisplay2( void );
//void spiTestPatern( void );
void spiSendVolume( void );
void spiChannelSelect(void);
void spiWrite( unsigned char );
#endif 