#include "pot_meter.h"


void initializeADC( void )
{
    ANSELbits.ANS7              = 0b1;                                          // Potentio meter, 1 = Analog
    
    ADCON1bits.ADFM             = 0b1;                                          // <Bit 7>      A/D Conversion Result Format Select bit, Right justified
    ADCON0bits.ADCS             = 0b10;                                         // <Bit 7-6>    A/D Conversion Clock Select bits, 10 = FOSC/32
    ADCON0bits.CHS              = 0b0111;                                       // <Bit 5-2>    Analog Channel Select bits, 0111 = AN7
    ADCON0bits.ADON             = 0b1;                                          // <bit 0>      ADC Enable bit, 1 = ADC is enabled
    PIE1bits.ADIE               = 0b0;                                          // ADC interrupt enable, 0 = disabled. 
    ADCON0bits.GO               = 0b1;                                          // 1 = Turn on ADC.
    T1CONbits.TMR1ON            = 0b1;                                          // 1 = Turn on Timer 1
    
    buildADCLookupTable();                                                      // Build the tables required for the adc
}



void buildADCLookupTable()
{
    ADC_TRANSLATION_TABLE_LOW[0] = ADC_BASE_MIN;                                // This is the min so yay
    
    for (int i = 0; i < ADC_MAX_VOLUME; i++)                                    // Make enough entries to hold all the volume steps
    {
        ADC_TRANSLATION_TABLE_LOW[i] = (( i * ADC_MAX_STEP_SIZE ) + ADC_DEADZONE_SIZE); // Save lowest value
        ADC_TRANSLATION_TABLE_HIGH[i] = ((( i + 1 ) * ADC_MAX_STEP_SIZE ) - ADC_DEADZONE_SIZE); // Save the highest value
    }
}

void setADCVolume( void )
{
    unsigned int ADC_VALUE  = ( ADRESH << 8 ) | ADRESL;                         // Get the complete adc value
    unsigned char i;
    for ( i = 0; i < ADC_MAX_VOLUME; i++ )                                      // Loop through all possible volumes
    {
        if ((ADC_TRANSLATION_TABLE_LOW[i] < ADC_VALUE ) && ( ADC_TRANSLATION_TABLE_HIGH[i] > ADC_VALUE )) // IS it between any of these?
        {
            ADC_STATE_REG = i;                                                  // This is our volume
            break;                                                              // Get outta here
        }
        if(ADC_VALUE > ADC_TRANSLATION_TABLE_LOW[ADC_MAX_VOLUME - 1])           // Is it bigger then the higest LOWEST number?
        {
            ADC_STATE_REG = ADC_MAX_VOLUME - 1;                                 // This is our volume
            break;                                                              // get outta here
        }
        if(ADC_VALUE < ADC_TRANSLATION_TABLE_HIGH[0])                           // IS it lower then our lowest HIGHEST number?
        {
            ADC_STATE_REG = 0;                                                  // This is our volume
            break;                                                              // Get outta here
        }
    }
}
