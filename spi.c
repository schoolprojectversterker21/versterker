#include <xc.h>
#include "spi.h"
#include "global_config.h"
#include "global_vars.h"


void initializeSPI()
{
    SSPCON = 0b00100000;                                                        // <7> Write Collision bit, not implemented
                                                                                // <6> Receive Overflow bit, not implemented
                                                                                // <5> Synchronous Serial Port bit, 1 = Enable SCK-, SDO-, SDO-lines for SPI. 
                                                                                // <4> Clock Polarity Select bit, 0 = idle state for clock is a low level
                                                                                // <3-0> Synchronous Serial Port Mode Select bits, 0000 = SPI Master mode, clock = Fosc/4

    SSPSTAT = 0b01000000;                                                       // <7> Sample bit, 0 = Data Sampled at middle of time.
                                                                                // <6> SPI clock edge bit, 0 = Data send on rising edge of CLK.
                                                                                // <5> not implemented
                                                                                // <4> not implemented
                                                                                // <3> not implemented
                                                                                // <2> not implemented
                                                                                // <1> not implemented
                                                                                // <0> Buffel full status bit, 0 = SSPBUF is empty
    
    
                                                                                // DISPLAYS
    TRISD = 0x00;                                                               // <7-0> 0 = RD0-7 as output 
    TRISC = 0x00;                                                               // <7-0> 0 = RC0-7 as output
    SSPBUF = 0x00;       
    
    spiInitDisplay2();                                                          // Initialize display 1
    spiInitDisplay1();                                                          // Initialize display 1
}

void spiWrite(unsigned char data)
{
    unsigned char x;                                                            // Temp var
    x = SSPBUF;                                                                 // Read variable as is required
    SSPBUF = data;                                                              // Copy data to SSPBUF
    while( !PIR1bits.SSPIF );                                                   // Wait till transmition has taken place
}


void spiInitDisplay1( void )
{
   
    PORTCbits.RC2 = 1;                                                          // Active Low, 1 = Reset off display 1
    PORTCbits.RC6 = 0;                                                          // 1 = Blank LED display 1.
    PORTCbits.RC4 = 1;                                                          // Active low, 0 = Select display 1
    PORTCbits.RC3 = 0;                                                          // SPI clock line, 0 = latch Register/dot select
    PORTCbits.RC7 = 0;                                                          // 0 = Dot register, 1 = Control Register display 1


    PORTCbits.RC7 = 1;                                                          // 0 = Dot register, 1 = Control Register display 1
    PORTCbits.RC4 = 0;                                                          // 0 = Chip enable Display 1
    spiWrite(0b01001100);                                                       // <7> 0 = Control word 0, 1 = Control word 1
                                                                                // <6> Sleep mode, 0 = enabled, 1 = disabled     
                                                                                // <5-4> Relative brightness, 00 = 73% (= 100%)
                                                                                // <3-0> PWM brightness Control, 1100 = 47% relative 
   
    PORTCbits.RC4 = 1;                                                          // Chip enable, 1 = Chip disable Display 1

    PORTCbits.RC7 = 1;                                                          // Control register select, 1 = Select Control Register
    PORTCbits.RC4 = 0;                                                          // 0 = Chip enable Display 1

    spiWrite(0b10000000);                                                       // <7> 0 = Control word 0, 1 = Control word 1
                                                                                // <6-2> unimplemented, must be set low
                                                                                // <1> External Display Oscillator Prescaler, 0 = 1:1, 1=1:8 
                                                                                // <0> Simultaneaous data Out, 1 = D_out is tied to D_in
    PORTCbits.RC4 = 1;                                                          // latch control word to register

    PORTCbits.RC7 = 0;                                                          // write to dot register

}

void spiInitDisplay2( void )
{
    PORTDbits.RD2 = 1;                                                          // Active Low, 1 = Reset off display 2
    PORTDbits.RD6 = 0;                                                          // 1 = Blank LED display 2.
    PORTDbits.RD4 = 1;                                                          // Active low, 0 = Select display 2
    PORTCbits.RC3 = 0;                                                          // SPI clock line, 0 = latch Register/dot select
    PORTDbits.RD7 = 0;                                                          // 0 = Dot register, 1 = Control Register display 2


    PORTDbits.RD7 = 1;                                                          // 0 = Dot register, 1 = Control Register display 2
    PORTDbits.RD4 = 0;                                                          // 0 = Chip enable Display 2
    spiWrite(0b01001100);                                                       // <7> 0 = Control word 0, 1 = Control word 1
                                                                                // <6> Sleep mode, 0 = enabled, 1 = disabled     
                                                                                // <5-4> Relative brightness, 00 = 73% (= 100%)
                                                                                // <3-0> PWM brightness Control, 1100 = 47% relative 

    PORTDbits.RD4 = 1;                                                          // Chip enable, 1 = Chip disable Display 2

    PORTDbits.RD7 = 1;                                                          // Control register select, 1 = Select Control Register display 2
    PORTDbits.RD4 = 0;                                                          // 0 = Chip enable Display 2

    spiWrite(0b10000000);                                                       // <7> 0 = Control word 0, 1 = Control word 1
                                                                                // <6-2> unimplemented, must be set low
                                                                                // <1> External Display Oscillator Prescaler, 0 = 1:1, 1=1:8 
                                                                                // <0> Simultaneaous data Out, 1 = D_out is tied to D_in
    
    PORTDbits.RD4 = 1;                                                          // 0 = Chip enable Display 2

    PORTDbits.RD7 = 0;                                                          // 0 = Dot register, 1 = Control Register display 2
}

void spiSendVolume( void )
{
    char charOne, charTwo;
    
    charOne = ADC_STATE_REG / 10;                                               // Filter 2nd digit (10^1)
    charTwo = ADC_STATE_REG % 10;                                               // Filter 1st digit (10^0)
    
    int que[ 8 ] = {CHAR_V, CHAR_O, CHAR_L, CHAR_SPACE, CHAR_SPACE, CHAR_SPACE, charOne, charTwo }; // Build what we want send
    PORTCbits.RC4 = 0;                                                          // Prepare display
    for(int i = 0; i < 8; i++)                                                  // 8 * 5 bytes
    {
        for( int x = 0; x < 5; x++)
        {
            spiWrite(FONT_TRANSLATION_TABLE[que[i]][x]);                        // Send the data
        }
    }
    PORTCbits.RC4 = 1;                                                          // Latch the data
}

void spiChannelSelect()
{
    unsigned char charOne;                                                      // Temp var

    charOne = (ROTARY_CHANNEL_COUNT >> 2) +1;                                   // Filter (n/4) part as channel number
    
    int que[ 8 ] = {CHAR_C, CHAR_H, CHAR_N, CHAR_L, CHAR_SPACE, CHAR_SPACE, CHAR_ZERO, charOne }; // Build what we want to send
    PORTDbits.RD4 = 0;                                                          // Prepare the display
    for(int i = 0; i < 8; i++)                                                  // 8 * 5 bytes
    {
        for( int x = 0; x < 5; x++)
        {
            spiWrite(FONT_TRANSLATION_TABLE[que[i]][x]);                        // Send data
        }
    }
    PORTDbits.RD4 = 1;                                                          // Latch the data
}