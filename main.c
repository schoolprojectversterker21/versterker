/* 
 * File:   main.c
 * Author: Robbert de Jel, Timo van der Steenhoven, Mark venneker.
 *
 * Created on November 22, 2018, 9:34 AM
 */

// CONFIG1
#pragma config FOSC             = EXTRC_CLKOUT// Oscillator Selection bits (RC oscillator: CLKOUT function on RA6/OSC2/CLKOUT pin, RC on RA7/OSC1/CLKIN)
#pragma config WDTE             = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE            = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE            = ON       // RE3/MCLR pin function select bit (RE3/MCLR pin function is MCLR)
#pragma config CP               = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD              = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN            = OFF      // Brown Out Reset Selection bits (BOR disabled)
#pragma config IESO             = OFF       // Internal External Switchover bit (Internal/External Switchover mode is disabled)
#pragma config FCMEN            = ON       // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)
#pragma config LVP              = OFF         // Low Voltage Programming Enable bit (RB3/PGM pin has PGM function, low voltage programming enabled)

// CONFIG2
#pragma config BOR4V = BOR40V   // Brown-out Reset Selection bit (Brown-out Reset set to 4.0V)
#pragma config WRT = OFF        // Flash Program Memory Self Write Enable bits (Write protection off)

#include <xc.h>
#include <pic16f887.h>
#include "global_config.h"
#include "global_macros.h"
#include "global_vars.h"
#include "rotary_encoder.h"
#include "pot_meter.h"
#include "spi.h"
#include "motor.h"
#include "ir.h"
#include "eeprom.h"




/*
    FUNCTIONS
 */
void initializeTimer1( void );

void initializeTimer1(){
    
    T1CONbits.TMR1CS = 0b0;     // Internal clock selected (Fosc/4)
    T1CONbits.T1CKPS = 0b00;    // prescaler 1:1, see TMR1 commentary
    PIE1bits.TMR1IE = 0b1;      // enable Timer 1 interrupt
    TMR1 = 55536;               
                                // Elke honderdste van een seconde=
                                // 1000000 / 100 = 10000;
                                // Hiervoor is een 1:1 prescaler te gebruiken, want 65535 + 1 - 10000 = 55536.
                                
                                // elke tiende van een seconde= 
                                // 1000000 instructies per seconde, TMR1 moet dus 1000000/10 = 100000 instructies wachten.
                                // 100000 / (65535 + 1) = 1,52 verhouding. Minimale prescaler van 1:2 nodig. 
                                // (65535+1) - (100000/2) = 15536 
    
                                // elke halve seconde = 
                                // 1000000 instructies per seconde, TMR1 moet dus 1000000/2 = 500000 instructies wachten.
                                // TMR1 heeft een grootte van 65535 + 1. 500000 / 65535 = ~7.6 verhouding
                                // De prescaler van TMR1 moet dus minimaal 8 bedragen. 
                                // (65535 + 1) - (500000 / 8 ) = 3035 beginwaarde van TMR1.
       
}


void __interrupt() ISR(void)
{
     
    if(INTCONbits.RBIF == 1)                                                    // Got an interrupt for the rotary encoder
    {
        setRotary();                                                            // get the states from the rotary encoder
        setRotaryLeds();                                                        // Set the corresponding leds.
        INTCONbits.RBIF = 0;                                                    // Reset interrupt flag
    }
    if(INTCONbits.INTF == 1)                                                    // IR interrupt flag
    {
        pollIR();                                                               // Get the correct action from the IR
        INTCONbits.INTF = 0;                                                    // Reset the interrupt flag.
    }
    if(PIR1bits.TMR1IF){                                                        // Did timer1 roll over?
        TMR1 = 55536;                                                           // Reset the timer to approx 1/10 of a second
        EEPROM_WATCHDOG++;                                                      // Increment the eeprom watchdog to see when we need to save
        if(EEPROM_WATCHDOG == 255)                                              // We reached the max, time to save!
        {
            saveData();                                                         // Save values to eeprom
            EEPROM_WATCHDOG = 0;                                                // Reset th eeprom watchdog timer
        }
        if(ADCON0bits.GO == 0)                                                  // See if the ADC is done
        {
            setADCVolume();                                                     // Set the volume
            spiSendVolume();                                                    // update the display
            ADCON0bits.GO = 1;                                                  // Enable the ADC
        }
        PIR1bits.TMR1IF = 0b0;                                                  // Reset the timer interrupt
    }
}


int main()
{
	 OSCCON              = 0b01100111;                                          // <7> unimplemented
                                                                                // <6-4> 110 = 4MHz clock frequency
                                                                                // <3> unimplemented
                                                                                // <2> 1 = internal oscillator stable bit (high Hz) enabled
                                                                                // <1> 1 = Internal oscillator stable bit (low Hz)enabled
                                                                                // <0> 1 = Internal oscillator used for system clock
    initializeVars();                                                           // Initialize variables ( See the file! )
    initializeRotary();                                                         // Initialize the rotary ( See the file! )
    initializeTimer1();                                                         // Initialize timers 1 ( See the file! )
    initializeADC();                                                            // Initialize the ADC ( See the file! )
    initializeSPI();                                                            // Initialize the SPI ( See the file! )
    initializeMotor();                                                          // Initialize the Motor ( See the file! )
    initializeIR();                                                             // Initialize the IR ( See the file! )
    
    INTCONbits.INTE     = 0b1;                                                  // 1 = External interrupt enable 
    INTCONbits.PEIE     = 0b1;                                                  // 1 = Peripheral; interrupt enable
    INTCONbits.GIE      = 0b1;                                                  // 1 = Global interrupt enable
    
	while(1)
    {
        asm("nop");                                                             // Waste some time as MPlab likes to throw a warning if we don't
    }
    return 0;
}