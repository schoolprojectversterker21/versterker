#ifndef _POT_FILE
#define _POT_FILE
#include "global_vars.h"
#include <pic16f887.h>

void initializeADC( void );
void buildADCLookupTable( void );
void setADCVolume( void );
#endif