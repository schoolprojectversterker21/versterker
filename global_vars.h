#ifndef _GLOBAL_VARS_FILE
#define _GLOBAL_VARS_FILE

#include "global_config.h"

void initializeVars( void );


/*
 *  Character array filled will all characters used in this project.
 */
unsigned const char FONT_TRANSLATION_TABLE[17][5] =
                                        {
                                            [CHAR_ZERO]     = { 0x3E, 0x51, 0x49, 0x45, 0x3E },
                                            [CHAR_ONE]      = { 0x00, 0x42, 0x7F, 0x40, 0x00 },
                                            [CHAR_TWO]      = { 0x62, 0x51, 0x49, 0x49, 0x46 },
                                            [CHAR_THREE]    = { 0x22, 0x41, 0x49, 0x49, 0x36 },
                                            [CHAR_FOUR]     = { 0x18, 0x14, 0x12, 0x7F, 0x10 },    
                                            [CHAR_FIVE]     = { 0x27, 0x45, 0x45, 0x45, 0x39 },
                                            [CHAR_SIX]      = { 0x3C, 0x4A, 0x49, 0x49, 0x30 },
                                            [CHAR_SEVEN]    = { 0x01, 0x71, 0x09, 0x05, 0x03 },
                                            [CHAR_EIGHT]    = { 0x36, 0x49, 0x49, 0x49, 0x36 },
                                            [CHAR_NINE]     = { 0x06, 0x49, 0x49, 0x29, 0x1E },
                                            [CHAR_V]        = { 0x07, 0x18, 0x60, 0x18, 0x07 },
                                            [CHAR_O]        = { 0x3E, 0x41, 0x41, 0x41, 0x3E },
                                            [CHAR_L]        = { 0x7F, 0x40, 0x40, 0x40, 0x40 },
                                            [CHAR_C]        = { 0x3E, 0x41, 0x41, 0x41, 0x22 },
                                            [CHAR_H]        = { 0x7F, 0x08, 0x08, 0x08, 0x7F },
                                            [CHAR_N]        = { 0x7F, 0x04, 0x08, 0x10, 0x7F },
                                            [CHAR_SPACE]    = { 0x00, 0x00, 0x00, 0x00, 0x00 }
                                        };

/*
 *  Translation table build with ranges to figure out the volume.
 */
unsigned short ADC_TRANSLATION_TABLE_LOW[ADC_MAX_VOLUME];
unsigned short ADC_TRANSLATION_TABLE_HIGH[ADC_MAX_VOLUME];

unsigned char   ROTARY_STATE_REG,
                ADC_STATE_REG,
                EEPROM_WATCHDOG;
signed char     ROTARY_CHANNEL_COUNT,
                ADC_STATE_FILTER;

#endif 