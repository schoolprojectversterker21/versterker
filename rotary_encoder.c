#include "rotary_encoder.h"
#include "global_vars.h"
#include "spi.h"
#include "eeprom.h"
#include <pic16f887.h>
#include <xc.h>

void initializeRotary()
{
    ANSEL                   = 0b10000000;                                       // <7> Set ANS7, RE2 as Analog
    ANSELH                  = 0x00;                                             // unimplemented
    
    OPTION_REGbits.nRBPU    = 0b0;                                              // PORTB Pull-up enable bit, 0 = enabled
    OPTION_REGbits.INTEDG   = 0b0;                                              // Interrupt Edge Select bit, 0 = falling edge
    
    INTCONbits.RBIF         = 0b0;                                              // PORTB Change interrupt bit, 0 = flag is cleared
    INTCONbits.INTF         = 0b0;                                              // External Intterupt bit, 0 = flag is cleared
    
    TRISBbits.TRISB4        = 0b1;                                              // 1 = RB4 as Input
    TRISBbits.TRISB5        = 0b1;                                              // 1 = RB5 as Input
    
    
    TRISAbits.TRISA0        = 0b0;                                              // Set LED 0 as output
    TRISAbits.TRISA1        = 0b0;                                              // Set LED 1 as output
    TRISAbits.TRISA2        = 0b0;                                              // Set LED 2 as output
    TRISAbits.TRISA3        = 0b0;                                              // Set LED 3 as output
    
    IOCBbits.IOCB4          = 0b1;                                              // Interrupt on change enable bit, 1 = RB4 IOC is enabled
    IOCBbits.IOCB5          = 0b1;                                              // Interrupt on change enable bit, 1 = RB5 IOC is enabled
    return;
}

    
void setRotary()
{
    unsigned char debounceA, debounceB;                                         // Temp variables
    for( int i = 0; i < 8; i++ )
    {
        debounceA = ( debounceA << 1 ) | PORTBbits.RB4;                         // Save the state
        debounceB = ( debounceB << 1 ) | PORTBbits.RB5;                         // Save the state
    }    
    
    ROTARY_STATE_REG = ( ROTARY_STATE_REG << 0b1 ) | ((debounceA & 0x0f) == 15);// is it all ones? if so, add it. otherwise do a 0.
    ROTARY_STATE_REG = ( ROTARY_STATE_REG << 0b1 ) | ((debounceB & 0x0f) == 15);// is it all ones? if so, add it. otherwise do a 0.
    return;
}

void setRotaryLeds( void )
{
     if((( ROTARY_STATE_REG >> 3 ) & 0b00000001 ) == (( ROTARY_STATE_REG & 0b00000001 )))         // Grab last B and A signal state 
    {
        ROTARY_CHANNEL_COUNT++;                                                 // Count upp
        if(ROTARY_CHANNEL_COUNT > 15){                                          // See if we reached your maximum
            ROTARY_CHANNEL_COUNT = 0;                                           // Reset if we did 
        }
        setChannelLED();                                                        // Update the LEDS
        spiChannelSelect();
        return;
    }
    ROTARY_CHANNEL_COUNT--;                                                     // Count down
    if(ROTARY_CHANNEL_COUNT < 0){                                               // Did we reach the min?
        ROTARY_CHANNEL_COUNT = 15;                                              // Reset to the max
    }                                                                           
    setChannelLED();                                                            // Update the leds
    spiChannelSelect();
    return;
}

void setChannelLED(){
    PORTA = 0x00;
    PORTA = ~(PORTA|((1<<(ROTARY_CHANNEL_COUNT >> 2))));
    return;
}